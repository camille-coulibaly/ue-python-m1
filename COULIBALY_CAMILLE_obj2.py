#Objectif 2:

import sys

try :
    fichier_input = sys.argv[1]
except:
    sys.exit ("Erreur: fichier non specifie")


def ecriture (ma_liste, mon_fichier):
    """
    input : liste remplie des id des AA[i] et AA[j] ainsi que leurs distances respectives + le nom du fichier dans lequel on souhaite stocker ses donnees
    output : fichier contenant les elements de la liste donnee en input
    """
    with open (mon_fichier, "w") as fichier: 
        for item in ma_liste:   
             fichier.write(item)
            

x = [] #initialisation d'une liste x qui stockera les positions des AA en x
y = [] #initialisation d'une liste y qui stockera les positions des AA en y  
z = [] #initialisation d'une liste z qui stockera les positions des AA en z
AA_id = []  # initialisation d'une liste AA_id qui referencera les noms des AA associes à leur numero
liste_distance = [] #initialisation de la liste_distance qui stockera les distances entre chaque AA

with open (fichier_input, "r") as filein: #ouverture du fichier_input en mode lecture
    for ligne in filein:   #pour chaque ligne du fichier, 
        nom_atome = ligne[12:16].strip()  #determination du nom de l'atome
        nom_AA = ligne[17:20].strip()  #determination du nom de l'AA
        nb_AA = int(ligne[22:26]) #determination du numero de l'AA
        if nom_atome == "CA":  #si l'atome est un carbone alpha
            AA_id.append(nom_AA + " " + str(nb_AA)) #la liste AA_id s'incremente du nom et du numero associe de l'AA
            x.append(float(ligne[30:38])) #x est incremente de la position en x de l'AA
            y.append(float(ligne[38:46])) # de meme pour y
            z.append(float(ligne[46:54])) #et pour z

    for i in range (len(x)):  #pour un iterateur i allant jusqu'a la longueur de la liste x
        for j in range (len(x)): #pour un iterateur j allant jusqu'a la longueur de la liste x
            if i != j and i<j: #si le numero de l'AA1 est different de celui del'AA2 et si nbAA1 < nbAA2
                dist = round((((x[i]-x[j])**2)+((y[i]-y[j])**2)+(z[i]-z[j])**2)**0.5, 3) #formule de la distance des coordonnees
                liste_distance.append (AA_id[i] + " - " + AA_id[j] + " : " + str(dist) + "\n") #la liste_distance est incrementee des id des AA[i] et [j] ainsi que de leur distance respective
    mes_elements = fichier_input.split(".") #separe les elements du nom du fichier_input au niveau du "."
    mon_fichier = mes_elements[0]+"_distances.txt" #nom du fichier qui va etre cree
    ecriture (liste_distance, mon_fichier) #appel de la fonction ecriture qui va generer un fichier contenant tous les elements de la liste_distance

