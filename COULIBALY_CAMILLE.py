#Objectif 1:

import sys 

fichier_input = sys.argv[1] #le fichier input sera precisé au moment du lancement du script.


def ecriture (ma_liste, mon_fichier):  #la fontion ecriture prend pour entrée 2 arguments, la liste remplie par une chaine et le nom du fichier ou cette liste sera ecrite.
    with open (mon_fichier, "w") as fichier:     #ouvre le fichier en mode ecriture
        for item in ma_liste:   #pour chaque objet dans la liste.
            fichier.write(item)    #ecrire dans le fichier cet objet.

liste_fichier = []   #initialisation d'une liste 


la_chaine = "A"  #initailisation de la chaine qui prend la valeur "A"

with open (fichier_input, "r") as filein: #ouvre le fichier input en mode lecture
    for ligne in filein:   #pour chaque ligne dans le fichier input
        if ligne[0:4]=="ATOM":  #si les colonnes de 0 à 3 inclus de la ligne correspondent à ATOM
            liste_fichier.append(ligne)  #incrementer cette ligne dans la liste
            if ligne[21]!= la_chaine:  #si la colonne 21 de la ligne est differente à la valeur de la variable la_ chaine
                print("Passage de la chaine",la_chaine, "a la chaine", ligne[21])   #afficher le changement de chaine de la valeur de la_chaine à la valeur de la colonne 21 de la ligne.
                mon_fichier ="3iwm_" + la_chaine + ".pdb"  #la variable mon_fichier correspond au nom du fichier qui va contenir la liste.
                ecriture(liste_fichier[:-1], mon_fichier) #utilisation de la fonction ecriture pour remplir le fichier avec les objets contenus dans la liste moins la derniere ligne de celle-ci (debut de la chaine suivante)
                liste_fichier = [] #reinitialisation de la liste
                liste_fichier.append(ligne)  #recupération de la derniere ligne non selectionnee dans le fichier precedent
            la_chaine = ligne[21]  #la_chaine prend la valeur de la colonne 21 de la ligne.
    print ("derniere chaine lue") #affiche le message derniere chaine lue, permet de se reperer.
    print (len(liste_fichier)) #affiche la longueur de la liste
    print (la_chaine)  #affiche la valeur de la variable la_chaine, il s'agit de la chaine E.
    #comme il n'y aura plus de changement de chaine, les conditions du if ne seront pas respectees, donc il faut rajouter une etape supplementaire pour avoir un fichier de la chaine E.
    mon_fichier = "3iwm_"+ la_chaine + ".pdb" #la variable mon_fichier correspond au nom du fichier qui va contenir la liste de chaine E.
    ecriture(liste_fichier, mon_fichier)   #utilisation de la fonction ecriture pour remplir le fichier avec les objets contenus dans la liste
    liste_fichier = []   #reinitialisation de la liste.
                
          
